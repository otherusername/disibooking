package edu.disi.booking.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.disi.booking.model.Booking;
import edu.disi.booking.model.MailText;
import edu.disi.booking.model.User;
import edu.disi.booking.ps.BookingDAO;
import edu.disi.booking.ps.MailService;

@RestController
public class BookingController {

	@Autowired
	private BookingDAO bookingDAO;
	/*@Autowired
	private UserDAO userDAO;
	*/

	/*@Autowired
	public JavaMailSender emailSender;
	*/
	
	
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value="/mybookings/{user_email}", method=RequestMethod.GET)
	public List<Booking> bookings(@PathVariable(value = "user_email") String email) {
		List<Booking> bookings = bookingDAO.findBooking(email);
		return bookings;
	}
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value="/hotelbookings", method=RequestMethod.GET)
	public List<Booking> listAllBbookings() {
		List<Booking> bookings = bookingDAO.listAllBookings();
		MailService mailService = new MailService("disi2019booking@gmail.com", "Disi2019");
		mailService.sendMail("norbert.nagyseres@gmail.com", "New DVD!", "bla");
		return bookings;
	}
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value="/email", method=RequestMethod.POST)
	public boolean sendmail(@RequestBody MailText mailbazd) {
		MailService mailService = new MailService("disi2019booking@gmail.com", "Disi2019");
		mailService.sendMail(mailbazd.getEmail(), "Meztelen allok elotted!", mailbazd.getText());
		return true;
	}

	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value = "/book", method = RequestMethod.POST)
	public void book(@RequestBody Booking booking) {
		bookingDAO.saveBooking(booking);
	}

	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value = "/bookings/{id}", method = RequestMethod.DELETE)
	public void cancelBooking(@PathVariable("id") int bookingId) {
		bookingDAO.deleteBooking(bookingId);
	}

	// TO BE EXTENDED
	/*
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/bookings/{id}", method = RequestMethod.PUT)
	public void updateBooking(@PathVariable("id") long bookingId, @RequestParam("nrNights") int nrNights) {
		Booking updatedBooking = bookingDAO.findBookingById(bookingId);
		updatedBooking.setNrNights(nrNights);
		bookingDAO.updateBooking(updatedBooking);
	}

	public void sendSimpleMessage(String to, String subject, String text) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setSubject(subject);
		message.setText(text);
		emailSender.send(message);
	}*/

}
