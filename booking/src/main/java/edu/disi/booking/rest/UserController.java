package edu.disi.booking.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.disi.booking.model.User;
import edu.disi.booking.ps.UserDAO;

@RestController
public class UserController {

	@Autowired
	UserDAO userDao;
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value="/login/{email}", method = RequestMethod.GET)
	public User loginUser(@PathVariable("email") String email) {
		return userDao.findByEmail(email);
	}
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value="/user/create", method = RequestMethod.POST)
	public void addUser(@RequestBody User user) {
		userDao.saveUser(user);
	}

}
