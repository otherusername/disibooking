package edu.disi.booking.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.disi.booking.model.Booking;
import edu.disi.booking.model.Hotel;
import edu.disi.booking.ps.HotelDAO;



@RestController
public class HotelController {
	
	@Autowired
	private HotelDAO hotelDAO;

	
	//find all hotels
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping("/hotels")
	public List<Hotel> hotels(){
		List<Hotel> hotels = hotelDAO.listAllHotels();
		return hotels;
	}
	
	//find all bookings of a hotel
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping("/hotelbookings")
	public List<Booking> hotelBookings(@RequestParam(value = "hotelName") String hotelName) {
		List<Booking> bookings = hotelDAO.findHotelBookings(hotelName);
		return bookings;
	}
	
	//SAVE HOTEL
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value = "/hotel", method = RequestMethod.POST)
	public void hotel(@RequestBody Hotel hotel) {
		hotelDAO.saveHotel(hotel);
	}
	
	//DELETE HOTEL
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value = "/hotels/{hname}",method = RequestMethod.DELETE)
	public void deleteHotel(@PathVariable("hname") String hname) {
		hotelDAO.deleteHotel(hname);
	}
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value = "/hotelu", method = RequestMethod.PUT)
	public void updateHotel(@RequestBody Hotel hotel) {
		hotelDAO.updateHotel(hotel);
	}
}