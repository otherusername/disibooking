package edu.disi.booking.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.disi.booking.model.Comment;
import edu.disi.booking.model.Hotel;
import edu.disi.booking.model.HotelRating;
import edu.disi.booking.model.User;
import edu.disi.booking.ps.CommentDAO;
import edu.disi.booking.ps.HotelDAO;
import edu.disi.booking.ps.UserDAO;

@RestController
public class CommentController {

	@Autowired
	CommentDAO commentDao;
	
	@Autowired
	HotelDAO hotelDao;
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value="/comment/create", method = RequestMethod.POST)
	public void addComment(@RequestBody Comment comment) {
		commentDao.saveComment(comment);
	}
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value="/stat", method = RequestMethod.GET)
	public List<HotelRating> statistcs() {
		List<Hotel> lhotel=hotelDao.listAllHotels();
		List<HotelRating> lhotelr=new ArrayList<HotelRating>();
		for(Hotel h:lhotel) {
			HotelRating hr=new HotelRating();
		List<Comment> lc=commentDao.findComments(h.getHname());
		float st=0;
		int i=0;
		int sum=0;
		for(Comment c :lc)
		{
			i++;
			sum+=c.getRating();
			
		}
		st=sum/i;
		int intst=(int) st;
		hr.setHotelname(h.getHname());
		hr.setRating(intst);
		lhotelr.add(hr);
		}
		return lhotelr;
	}
	
	
	@CrossOrigin(origins = "http://localhost:3100")
	@RequestMapping(value="/stat/{hotel_name}", method = RequestMethod.GET)
	public float statistcs(@PathVariable(value = "hotel_name") String hotel_name) {
		List<Comment> lc=commentDao.findComments(hotel_name);
		float st=0;
		int i=0;
		int sum=0;
		for(Comment c :lc)
		{
			i++;
			sum+=c.getRating();
			
		}
		st=sum/i;
		return st;
	}
	

}
