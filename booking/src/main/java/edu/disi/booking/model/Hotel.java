package edu.disi.booking.model;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="hotel")
public class Hotel {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "hotelID")
    private Integer hotelID;
 

	@Column(name="ownerID", nullable=false)
	private Integer ownerID;
		
	@Column(name="hname", nullable=false)
	private String hname;
	
	@Column(name="address", nullable=false)
	private String address;
	
	@Column(name="rating", nullable=false)
	private Integer rating;
	
	@Column(name="image", nullable=false)
	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getHotelID() {
		return hotelID;
	}

	public void setHotelID(Integer hotelID) {
		this.hotelID = hotelID;
	}

	public Integer getOwnerID() {
		return ownerID;
	}

	public void setOwnerID(Integer ownerID) {
		this.ownerID = ownerID;
	}

	public String getHname() {
		return hname;
	}

	public void setHname(String hname) {
		this.hname = hname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	
	


}