package edu.disi.booking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="comment")
public class Comment {


	
	    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	    @Column(name="id")
	    private int id;
	    
	    
	    public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		@Column(name = "hotel_name")
	    private String hotel_name;
	 
		@Column(name="text", nullable=false)
		private String text;
			
		@Column(name="rating", nullable=false)
		private int rating;


		public String getHotel_name() {
			return hotel_name;
		}

		public void setHotel_name(String hotel_name) {
			this.hotel_name = hotel_name;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public int getRating() {
			return rating;
		}

		public void setRating(int rating) {
			this.rating = rating;
		}
	
	}

