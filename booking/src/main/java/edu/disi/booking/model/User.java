package edu.disi.booking.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="user")
public class User {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "userID")
    private Integer userID;
 

	@Column(name="username", nullable=false)
	private String username;
		
	@Column(name="upassword", nullable=false)
	private String upassword;
	
	@Column(name="email", nullable=false)
	private String email;
	
	@Column(name="role", nullable=false)
	private String role;

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUpassword() {
		return upassword;
	}

	public void setUpassword(String upassword) {
		this.upassword = upassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}



	
}