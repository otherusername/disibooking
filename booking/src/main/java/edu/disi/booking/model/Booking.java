package edu.disi.booking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="booking")
public class Booking {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "bookingID")
    private Integer bookingID;

	@Column(name="userID", nullable=false)
	private Integer userID;
	
	@Column(name="user_email", nullable=false)
	private String user_email;
	
	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="tel", nullable=false)
	private String tel;

	@Column(name="arriving_date", nullable=false)
	private String arriving_date;
	
	@Column(name="nr_nopti", nullable=false)
	private Integer nr_nopti;
	
	@Column(name="hotel_name", nullable=false)
	private String hotel_name;
	
	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Integer getNr_nopti() {
		return nr_nopti;
	}

	public void setNr_nopti(Integer nr_nopti) {
		this.nr_nopti = nr_nopti;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public Integer getBookingID() {
		return bookingID;
	}

	public void setBookingID(Integer bookingID) {
		this.bookingID = bookingID;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	
	public String getArriving_date() {
		return arriving_date;
	}

	public void setArriving_date(String arriving_date) {
		this.arriving_date = arriving_date;
	}

}
