package edu.disi.booking.model;

public class MailText {
	private String email;
	private String text;
	
	public MailText() {super();}
	
	public MailText(String a,String b) {
		this.email=a;
		this.text=b;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	

}
