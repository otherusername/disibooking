package edu.disi.booking.model;

public class HotelRating {
	
	private String hotelname;
	
	private int rating;
	
	public HotelRating() {super();}

	
	public HotelRating(String a,int b) {
		this.hotelname=a;
		this.rating=b;
	}
	public String getHotelname() {
		return hotelname;
	}

	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	
	

}
