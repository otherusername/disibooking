package edu.disi.booking.ps;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.disi.booking.model.User;

@Transactional
@Repository
public class UserDAO {
	
	@PersistenceContext	
	private EntityManager entityManager;	
	
	public User findByEmail(String email) {
		return (User) entityManager.createQuery("from User where email = :email").setParameter("email", email).getResultList().get(0);
	}
	
	public void saveUser(User user) {
		entityManager.persist(user);
	}

}
