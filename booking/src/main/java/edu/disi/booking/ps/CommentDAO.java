package edu.disi.booking.ps;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.disi.booking.model.Booking;
import edu.disi.booking.model.Comment;
import edu.disi.booking.model.User;

@Transactional
@Repository
public class CommentDAO {
	
	@PersistenceContext	
	private EntityManager entityManager;	
	
	public void saveComment(Comment comment) {
		entityManager.persist(comment);
	}
	
	@SuppressWarnings("unchecked")
	public List<Comment> findComments(String hotel_name) {
		return entityManager.createQuery("from Comment where hotel_name = :hotel_name").setParameter("hotel_name", hotel_name).getResultList();
	}

}
