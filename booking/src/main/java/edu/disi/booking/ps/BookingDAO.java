package edu.disi.booking.ps;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.disi.booking.model.Booking;
import edu.disi.booking.model.Hotel;

@Transactional
@Repository
public class BookingDAO {

		@PersistenceContext	
		private EntityManager entityManager;	
		
		public void saveBooking(Booking booking) {
			entityManager.persist(booking);
		}
		
		@SuppressWarnings("unchecked")
		public List<Booking> findBooking(String email) {
			return entityManager.createQuery("from Booking where user_email = :user_email").setParameter("user_email", email).getResultList();
		}
		
		@SuppressWarnings("unchecked")
		public List<Booking> listAllBookings() {
			return entityManager.createQuery("from Booking").getResultList();
		}
		
		public Booking findBookingById(Long id) {
			return (Booking) entityManager.createQuery("from Booking where id = :id").setParameter("id", id).getResultList().get(0);
		}
		
		public void deleteBooking(int id) {
			this.entityManager.createQuery("delete from Booking where bookingID = :id").setParameter("id", id).executeUpdate();
		}
	
		public void updateBooking(Booking booking) {
			this.entityManager.merge(booking);
		}
}
