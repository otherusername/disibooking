package edu.disi.booking.ps;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.disi.booking.model.Booking;
import edu.disi.booking.model.Hotel;



@Transactional
@Repository
public class HotelDAO {

	@PersistenceContext	
	private EntityManager entityManager;	
	
	public void saveHotel(Hotel hotel) {
		entityManager.persist(hotel);
	}
	
	public Hotel findHotelById(Long id) {
		return (Hotel) entityManager.createQuery("from Hotel where id = :id").setParameter("id", id).getResultList().get(0);
	}
	
	public Hotel findHotelByName(String name) {
		return (Hotel) entityManager.createQuery("from Hotel where hname = :name").setParameter("name", name).getResultList().get(0);
	}
	
	public void deleteHotel(String hname) {
		this.entityManager.createQuery("delete from Hotel where hname = :hname").setParameter("hname", hname).executeUpdate();
	}
	
	public Hotel updateHotel(Hotel hotel) {
		Hotel h1=findHotelByName(hotel.getHname());
		h1.setOwnerID(hotel.getOwnerID());
		h1.setAddress(hotel.getAddress());
		h1.setRating(hotel.getRating());	
		h1.setImage(hotel.getImage());
		return this.entityManager.merge(h1);
	}
	
	@SuppressWarnings("unchecked")
	public List<Hotel> listAllHotels() {
		return entityManager.createQuery("from Hotel").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Booking> findHotelBookings(String hotelName) {
		return entityManager.createQuery("from Booking where hotelName = :hotelName").setParameter("hotelName", hotelName).getResultList();
	}
}