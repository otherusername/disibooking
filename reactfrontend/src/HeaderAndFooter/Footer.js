import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
    constructor() {
        super();
    };

    render() {
        return (
            <div className="footerPanel">
                <div className="footerText">
                    &#169; CopyRight Disi Hotel Booking
                </div>
            </div>
        );
    }
}
export default Footer;
