import React from 'react';
import {style} from "./Home.css";
import {Carousel} from "./Carousel";
import {LoginPanel} from "./LoginPanel";

export class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="home">
                <Carousel/>
                <LoginPanel/>
            </div>
        );
    }

}




