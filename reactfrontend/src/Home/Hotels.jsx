import React, { Component } from 'react';
import {style} from './Hotels.css';

export class Hotels extends Component {
    constructor() {
        super();
        this.state = {
            hotels: []
        };
    }

    componentDidMount() {
        let request = new XMLHttpRequest();
        request.open('GET', `http://localhost:8080/hotels`, true);

        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                let hotelsList = JSON.parse(request.response);
                this.setState({hotels: hotelsList});
            }
        };

        request.send();
    }

    renderRating(rating){
        let nrYellowStars = rating;
        let hearts = [];


        for (let i = 0; i < nrYellowStars; i++) {
            hearts.push(<img src={require('./no.png')} width="20" height="20"/>)
        }
        return (<div> {hearts} </div>)
    }

    render() {

        return (
            <div className="hotels-wrapper">
                {
                    this.state.hotels.map(
                        hotel => <div className="hotel-wrapper">
                            <div className="hotel-element">
                                <img src={hotel.image} alt={hotel.hname} width="300" height="200"/>
                            </div>
                            <div>
                                <br/>   <div className="hotel-label"> Hotel: </div> <span> {hotel.hname} </span>
                                <br/> <div className="hotel-label"> Address: </div> <span> {hotel.address} </span>
                                <br/> <div className="hotel-label"> Current rating: </div> <span> {this.renderRating(hotel.rating)} </span>
                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}


