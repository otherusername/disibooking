import React from 'react';
import {style} from './LoginPanel.css';
import {Redirect} from 'react-router-dom';

export class LoginPanel extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            error: false,
            loading: true,
            email: '',
            password: '',
            redirect: false
        };
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.login = this.login.bind(this);
        this.register = this.register.bind(this);
    }

    onChangeEmail(e){
        this.setState({email: e.target.value})
    }

    onChangePassword(e){
        this.setState({password: e.target.value})
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            if(this.state.email !== '') {

                if(this.state.email === 'admin@mail.com'){
                    sessionStorage.setItem("loggedAdmin", "true");
                    sessionStorage.setItem("loggedUser", 'admin@mail.com');
                    return <Redirect to={`/admin`}/>
                }else{
                    sessionStorage.setItem("loggedUser", `${this.state.email}`);
                    sessionStorage.setItem("clientLogged", 'true');
                    return <Redirect to={`/user/${this.state.email}`}/>
                }

            }else{
                return <Redirect to={`/usernew`}/>
            }
        }
    };

    login(){
        let request = new XMLHttpRequest();
        console.log(this.state.email);
        request.open('GET', `http://localhost:8080/login/${this.state.email}`, true);

        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                let usr = JSON.parse(request.response);
                console.log(usr);
                if(usr !== undefined) {
                    console.log(usr);
                    console.log(this.state.password);
                    if (usr.upassword === this.state.password) {
                        sessionStorage.setItem("email", this.state.email);
                        console.log(this.state.email);
                        this.setState({
                            redirect: true
                        });
                    }
                    this.setState({loading: false});
                }else{
                    this.setState({error: true});
                }
            }
        };

        request.send();
    }

    register(){
        this.setState({
            redirect: true
        });
    }

    render(){
        return(
            <div className="formHome">
                <div className="inputFieldHome">
                    Email: <input
                    className="inputFieldHome"
                    type="text"
                    ref="username"
                    placeholder="enter your email address"
                    onChange={this.onChangeEmail}/>
                </div>
                <br/>
                <div className="inputFieldHome">
                    Password: <input className="inputFieldHomePassword"
                                     type="password"
                                     ref="password"
                                     placeholder="enter your password"
                                     onChange={this.onChangePassword}
                />
                    <br/>
                </div>
                <div className="button-container">
                    <button className="loginButtonHome" onClick={this.login}> Login </button>

                    {
                        this.state.error &&
                        <div className="error-message"> Looks like something went wrong! </div>
                    }

                    <div className="register-text">
                        Don't have an account yet? Register now to book your dream vacation!
                    </div>
                    <button className="loginButtonHome" onClick={this.register}> Register </button>
                </div>
                {this.renderRedirect()}
            </div>
        );
    }
}
