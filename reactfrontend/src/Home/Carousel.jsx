import React from 'react';
import style from './Carousel.css';

const landingPictures = [{path: "./pics/landing3.jpg"}, {path: "./pics/landing2.jpg"}, {path: "./pics/landing1.jpg"}];

export class Carousel extends React.Component{
    constructor(props){
        super(props);
        this.state={};
    }

    render(){
        return (
            <div>
                <div className="carousel">
                    <ul className="slides">
                        <input type="radio" name="radio-buttons" id="img-1" checked/>
                        <li className="slide-container">
                            <div className="slide-image">
                                <img
                                    src="https://wallpapertag.com/wallpaper/full/7/9/9/788472-mansion-wallpapers-2560x1600-for-samsung.jpg"/>
                            </div>
                            <div className="carousel-controls">
                                <label htmlFor="img-3" className="prev-slide">
                                    <span>&lsaquo;</span>
                                </label>
                                <label htmlFor="img-2" className="next-slide">
                                    <span>&rsaquo;</span>
                                </label>
                            </div>
                        </li>
                        <input type="radio" name="radio-buttons" id="img-2"/>
                        <li className="slide-container">
                            <div className="slide-image">
                                <img src="https://cn.pling.com/img/9/2/4/7/e29f190e5dff9ed738089b1a98f409ef2afb.jpg"/>
                            </div>
                            <div className="carousel-controls">
                                <label htmlFor="img-1" className="prev-slide">
                                    <span>&lsaquo;</span>
                                </label>
                                <label htmlFor="img-3" className="next-slide">
                                    <span>&rsaquo;</span>
                                </label>
                            </div>
                        </li>
                        <input type="radio" name="radio-buttons" id="img-3"/>
                        <li className="slide-container">
                            <div className="slide-image">
                                <img
                                    src="https://cn.pling.com/img/9/2/4/7/e29f190e5dff9ed738089b1a98f409ef2afb.jpg"/>
                            </div>
                            <div className="carousel-controls">
                                <label htmlFor="img-2" className="prev-slide">
                                    <span>&lsaquo;</span>
                                </label>
                                <label htmlFor="img-1" className="next-slide">
                                    <span>&rsaquo;</span>
                                </label>
                            </div>
                        </li>
                        <div className="carousel-dots">
                            <label htmlFor="img-1" className="carousel-dot" id="img-dot-1"></label>
                            <label htmlFor="img-2" className="carousel-dot" id="img-dot-2"></label>
                        </div>
                    </ul>
                </div>
            </div>
        );
    }
}


