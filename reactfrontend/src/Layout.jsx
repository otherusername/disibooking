import React from 'react';
import Header from './HeaderAndFooter/Header';
import Footer from './HeaderAndFooter/Footer';
import logo from './pics/gradient-wave-hi.png'

let links = [
    {label: 'Home', link: 'home', active: true},
    {label: 'Hotels', link: 'hotels'},
    {label: 'Statistics', link: 'statistics'}
];

export class Layout extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div className="layout">
                <Header links={links}  logo={logo} {...this.props}/>
                <div className="layout-content" id="layout-content">
                    {this.props.children}
                </div>
                <Footer/>
            </div>
        );
    }

}
