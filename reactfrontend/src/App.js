import React, { Component } from 'react';
import './App.css';
import {AppRoutes} from "./AppRoutes";

class App extends Component {
  render() {
    return (
        <AppRoutes/>
    );
  }
}

export default App;
