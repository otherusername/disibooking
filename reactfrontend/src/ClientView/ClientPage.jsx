import React from 'react';
import {style} from "./ClientPage.css";
import {Redirect} from "react-router";
import {ReservationForm} from "./ReservationForm";

export class ClientPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            hotels: [],
            comment: '',
            rating: '',
            hotel_name: '',
            showReservations: false
        };


        this.onLogout = this.onLogout.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
        this.onListReservations = this.onListReservations.bind(this);
        this.changeComment = this.changeComment.bind(this);
        this.changeRating = this.changeRating.bind(this);
        this.onChangeHotel = this.onChangeHotel.bind(this);
        this.submit = this.submit.bind(this);
        this.resetRatingFormData = this.resetRatingFormData.bind(this);
        this.addHospitalsOptions = this.addHospitalsOptions.bind(this);
    }

    componentDidMount() {
        let request = new XMLHttpRequest();
        request.open('GET', `http://localhost:8080/hotels`, true);

        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                let hotelsList = JSON.parse(request.response);
                this.setState({hotels: hotelsList});
            }
        };

        request.send();
    }

    resetRatingFormData() {
        this.setState({comment: ''});
        this.setState({rating: ''});
        this.setState({hotel_name: ''});

    }

    renderRedirect = () => {
        if (this.state.redirect) {
            if (this.state.showReservations){
                return <Redirect to={`/myreservations`}/>
            }
            return <Redirect to={`/home`}/>
        }
    };

    onLogout(){
        this.setState({
            redirect: true
        });
        sessionStorage.setItem("clientLogged", "false");
    }

    onListReservations() {
        this.setState({
            redirect: true,
            showReservations: true
        });
    }

    addHospitalsOptions(){
        let x = document.getElementById("hotelSelect2");
        for (let i = 0; i < x.length; i++) {
            x.options[i] = null;
        }
        this.state.hotels.forEach(function(hotel){
            let option = document.createElement("option");
            option.text = hotel.hname;
            option.value = hotel.hname;
            console.log(option);
            x.add(option, x[0]);
        });
    }

    changeComment(e) {
        this.setState({comment: e});
    }

    changeRating(e) {
        this.setState({rating: e});
    }

    onChangeHotel(e) {
        console.log(e);
        this.setState({hotel_name: e});
    }

    submit() {
        let request = new XMLHttpRequest();
        request.open('POST', `http://localhost:8080/comment/create`, false);
        request.setRequestHeader('content-type', 'application/json');

        let name = this.state.hotel_name;
        let text = this.state.comment;

        let body = {
            hotel_name: name,
            text: text,
            rating: this.state.rating
        };

        request.send(JSON.stringify(body));

        this.resetRatingFormData();
    }

    render() {
        return (
            <div>
                {sessionStorage.getItem('loggedUser') === this.props.match.params.email && sessionStorage.getItem('clientLogged') === 'true' &&
                <div className="user-page">
                    <div className="logoutDiv">
                    <button className="logoutBtn btn" onClick={this.onLogout}> Log out</button>
                    </div>

                    <ReservationForm/>
                    <button className="list-reservations-btn btn" onClick={this.onListReservations}> List my bookings
                    </button>

                    <div className="onlyRating">
                        <h1 className="rating-title">
                            Evaluate your hotel
                        </h1>
                        <div className="selector">
                            <h3 className="text">Please select a hotel</h3>

                            <select ref="selector" id="hotelSelect2" onFocus={() => {
                                this.addHospitalsOptions()
                            }} onChange={e => this.onChangeHotel(e.target.value)}>
                                <option value="0">Please select a hotel</option>
                            </select>

                        </div>
                        <div className="textarea-wrapper">
                            <h3 className="text">Enter your comment here</h3>
                            <textarea className="textarea"
                                      placeholder={"Comment here"}
                                      value={this.state.comment}
                                      onChange={e => this.changeComment(e.target.value)}
                            />
                        </div>
                        <div className="ratingfield">
                            <h3 className="text">Enter a rating</h3>
                            <select ref="selector" onChange={e => this.changeRating(e.target.value)}>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <button className="addbutton btn" onClick={this.submit}>Add</button>
                    </div>

                </div>


                }
                {this.renderRedirect()}
            </div>
        );
    }

}




