import React from 'react';
import {style} from "./ReservationForm.css";
import {Redirect} from "react-router";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker-cssmodules.css"

import {isValidEmail, isValidPhoneNumber, isValidNumber} from "../Validation";

let dateS = '';

export class ReservationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false,
            succes: false,
            redirect: false,
            reservation: {
                userID: '1',
                arriving_date: '',
                user_email: '',
                name: '',
                tel: '',
                nr_nopti: '',
                hotel_name: ''
            },
            start_date: new Date(),
            reservationNumber: '',
            hotels: []
        };


        this.onBack = this.onBack.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeTel = this.onChangeTel.bind(this);
        this.onChangeArrivingDate = this.onChangeArrivingDate.bind(this);
        this.onChangeNrNights = this.onChangeNrNights.bind(this);
        this.onChangeHotelName = this.onChangeHotelName.bind(this);
        this.onChangeReservationNumber = this.onChangeReservationNumber.bind(this);
        this.deleteReservation=this.deleteReservation.bind(this);
        this.onMakeReservation = this.onMakeReservation.bind(this);
        this.onChangeStartDate = this.onChangeStartDate.bind(this);
        this.addHospitalsOptions = this.addHospitalsOptions.bind(this);
        this.resetReservationFormData = this.resetReservationFormData.bind(this);
        this.resetCancelReservationFormData = this.resetCancelReservationFormData.bind(this);

    }

    resetReservationFormData() {
        this.setState({
            reservation: {
                userID: '1',
                arriving_date: '',
                user_email: '',
                name: '',
                tel: '',
                nr_nopti: '',
                hotel_name: ''
            }
        })
    }

    resetCancelReservationFormData() {
        this.setState({reservationNumber: ''})
    }

    onChangeStartDate(date){
    let datee = date.toString();
    this.setState({start_date: datee});
    this.setState(prevState => ({
        reservation: {
            ...prevState.reservation,
            arriving_date: datee
        }
    }));
    }


    onChangeName(value) {
        this.setState(prevState => ({
            reservation: {
                ...prevState.reservation,
                name: value
            }
        }));
    }

    onChangeEmail(value) {
        this.setState(prevState => ({
            reservation: {
                ...prevState.reservation,
                user_email: value
            }
        }));
    }

    onChangeTel(value) {
        this.setState(prevState => ({
            reservation: {
                ...prevState.reservation,
                tel: value
            }
        }));
    }

    onChangeArrivingDate(value) {
        this.setState(prevState => ({
            reservation: {
                ...prevState.reservation,
                arriving_date: value
            }
        }));
    }

    onChangeNrNights(value) {
        this.setState(prevState => ({
            reservation: {
                ...prevState.reservation,
                nr_nopti: value
            }
        }));
    }

    onChangeHotelName(value) {
        console.log(value);
        this.setState(prevState => ({
            reservation: {
                ...prevState.reservation,
                hotel_name: value
            }
        }));
    }

    onChangeReservationNumber(value){
        this.setState({reservationNumber: value});
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={`/home`}/>
        }
    };

    onBack() {
        this.setState({
            redirect: true
        });
        sessionStorage.setItem("loggedUser", "false");
    }

    onMakeReservation() {

        if( isValidEmail(this.state.reservation.user_email) &&
            isValidNumber(this.state.reservation.nr_nopti)
        ) {
            let request = new XMLHttpRequest();
            request.open('POST', `http://localhost:8080/book`, false);
            request.setRequestHeader('content-type', 'application/json');
            request.send(JSON.stringify(this.state.reservation));
            this.resetReservationFormData();
            this.setState({succes: true});
        }
        else{
            this.setState({error: true});
        }
    }

    deleteReservation(){
        if(isValidNumber(this.state.reservationNumber)) {
            let request = new XMLHttpRequest();
            request.open('DELETE', `http://localhost:8080/bookings/${this.state.reservationNumber}`, false);
            request.setRequestHeader('content-type', 'application/json');
            request.send();
            this.resetCancelReservationFormData();
            this.setState({succes: true});
            this.setState({error: false});

        } else{
            this.setState({error: true});
        }
    }

    addHospitalsOptions(){
        let x = document.getElementById("hotelSelect");
        for (let i = 0; i < x.length; i++) {
            x.options[i] = null;
        }
        this.state.hotels.forEach(function(hotel){
            let option = document.createElement("option");
            option.text = hotel.hname;
            option.value = hotel.hname;
            x.add(option, x[0]);
        });
    }

    componentDidMount() {
        let request = new XMLHttpRequest();
        request.open('GET', `http://localhost:8080/hotels`, true);

        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                let hotelsList = JSON.parse(request.response);
                this.setState({hotels: hotelsList});
                console.log(hotelsList);
            }
        };

        request.send();
    }


    render() {
        return (
            <div className="allClientPage">

                {this.state.error && <div className="error-message msg"> Some fields are invalid! </div>}
                {this.state.success && <div className="success-message msg"> Your reservation was booked successfully! </div>}

                <div className="reservation-form-wrapper">
                    <h3  className="reservation-title">
                        Complete the fields, to book your holiday!
                    </h3>
                    <div className="reservation-form">

                        <img src="https://us.123rf.com/450wm/unkreatives/unkreatives1708/unkreatives170800072/84368917-stock-vector-illustration-of-a-vacation-and-travel-booking-concept-hand-over-tablet-presses-book-button-eps10-vec.jpg?ver=6"
                             width="450"
                             height="315"
                        />

                        <div>

                            <div className="inputField">
                                <div className="label-c"> Name: </div>
                                <input
                                    className="inputField"
                                    type="text"
                                    ref="name"
                                    value={this.state.reservation.name}
                                    onChange={e => this.onChangeName(e.target.value)}/>
                            </div>

                            <br/>

                            <div className="inputField">
                                <div className="label-c"> Email: </div>
                                <input
                                    className="inputField"
                                    type="text"
                                    ref="name"
                                    value={this.state.reservation.user_email}
                                    onChange={e => this.onChangeEmail(e.target.value)}/>
                            </div>

                            <br/>

                            <div className="inputField">
                                <div className="label-c"> Phone number: </div>
                                <input
                                    className="inputField"
                                    type="text"
                                    ref="name"
                                    value={this.state.reservation.tel}
                                    onChange={e => this.onChangeTel(e.target.value)}/>
                            </div>

                            <br/>

                            <div className="inputField">
                                <div className="inputField">
                                    Arriving Date:
                                <br/>
                                <DatePicker
                                    value = {this.state.reservation.arriving_date}
                                    dateFormat="YYYY/MM/DD"
                                    selected={this.state.startDate}
                                    onChange={this.onChangeStartDate}
                                />
                                </div>
                            </div >

                            <br/>

                            <div className="inputField">
                                <div className="label-c"> Number of nights: </div>
                                <input
                                    className="inputField"
                                    type="number"
                                    ref="name"
                                    value={this.state.reservation.nr_nopti}
                                    onChange={e => this.onChangeNrNights(e.target.value)}/>
                            </div>

                            <br/>

                            <div className="inputField">
                                <div className="label-c">
                                    Hotel Name:
                                <br/>
                                <select ref="selector" id="hotelSelect" onFocus={() => {
                                    this.addHospitalsOptions()
                                }} onChange={e => this.onChangeHotelName(e.target.value)}>
                                    <option value="0">Please select a hotel</option>
                                </select>
                                </div>
                            </div>

                            <br/>

                            <button className="reservation-btn" onClick={this.onMakeReservation}> Confirm </button>

                            <br/>

                        </div>
                    </div>
                    <div>
                        <h3  className="reservation-title">
                            Please enter your reservation number to cancel the reservation!
                        </h3>

                        <div className="reservation-form">

                            <div className="reservation-fieldss">
                            <div className="label-cancel"> Reservation number: </div>
                            <input
                                className="fild-cancel"
                                type="text"
                                ref="name"
                                value={this.state.reservationNumber}
                                onChange={e => this.onChangeReservationNumber(e.target.value)}/>
                            <div>
                                <button className="cancel-reservation-btn" onClick={this.deleteReservation}> Cancel reservation </button>
                            </div>
                            </div>
                            <div className="cancel-image">
                            <img src="https://www.novabookkeeping.com.au/wp-content/uploads/2016/11/Cancel-ABN.jpg"
                                 width="400"
                                 height="150"
                            />
                            </div>
                        </div>

                    </div>
                </div>
                {this.renderRedirect()}
            </div>
        );
    }

}




