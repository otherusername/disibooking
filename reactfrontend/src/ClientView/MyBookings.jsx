import React from 'react';
import {style} from "./MyBookings.css";
import {Redirect} from "react-router";


export class MyBookings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            my_reservations: []
        };

    }

    componentDidMount() {
        let email = sessionStorage.getItem("loggedUser");
        console.log(email);
        let request = new XMLHttpRequest();
        request.open('GET', `http://localhost:8080/mybookings/${email}`, false);
        request.setRequestHeader('content-type', 'application/json');

        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                let reservations = JSON.parse(request.response);
                this.setState({my_reservations: reservations});
            }
        };

        request.send();
    }

    render() {

        return (
        <div className="my-reservations-wrapper">
            <h2 className="reservations-title"> Your reservations </h2>
            <table className="reservations-table">
                <tr className="row">
                    <th>Reservation number</th>
                    <th>Hotel name</th>
                    <th>User email</th>
                    <th>Number of nights</th>
                </tr>
                {this.state.my_reservations.map(
                    reservation => <tr className="row">
                        <td className="data">{reservation.bookingID}</td>
                        <td className="data">{reservation.hotel_name}</td>
                        <td className="data">{reservation.user_email}</td>
                        <td className="data">{reservation.nr_nopti}</td>
                    </tr>
                )}
            </table>
        </div>
        );
    }

}



