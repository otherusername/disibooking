import React from 'react';
import {style} from "./Register.css";
import {Redirect} from "react-router";


export class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                username: '',
                upassword: '',
                email: '',
                role: 'user'
            },
            redirect: false
        };

        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSaveChanges = this.onSaveChanges.bind(this);
        this.onLogout = this.onLogout.bind(this);
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={`/home`}/>
        }
    };

    onLogout(){
        this.setState({
            redirect: true
        });
        sessionStorage.setItem("loggedUser", "false");
    }

    onSaveChanges(){
        let request = new XMLHttpRequest();
        request.open('POST', `http://localhost:8080/user/create`, true);
        request.setRequestHeader('content-type', 'application/json');

        request.send(JSON.stringify(this.state.user));
    }

    onChangeUsername(value) {
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                username: value
            }
        }));
    }

    onChangePassword(value) {
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                upassword: value
            }
        }));
    }

    onChangeEmail(value) {
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                email: value
            }
        }));
    }

    render() {

        return (
            <div>
                <div className="profile-wrapper">

                    <img className="profile-picture"
                         src={"https://www.sketchappsources.com/resources/source-image/profile-illustration-gunaldi-yunus.png"}
                         width="300"
                         height="300"
                    />

                    <div className="profile">

                        <div className="inputFieldProfile">
                            Username: <input
                            className="inputFieldProfile"
                            type="text"
                            ref="name"
                            placeholder="enter your username"
                            value={this.state.user.username}
                            onChange={e => this.onChangeUsername(e.target.value)}/>
                        </div>
                        <br/>

                        <div className="inputFieldProfile">
                            Password: <input
                            className="inputFieldProfile"
                            type="password"
                            ref="name"
                            placeholder="enter your password"
                            value={this.state.user.upassword}
                            onChange={e => this.onChangePassword(e.target.value)}/>
                        </div>
                        <br/>

                        <div className="inputFieldProfile">
                            Email: <input
                            className="inputFieldProfile"
                            type="text"
                            ref="name"
                            placeholder="enter your email address"
                            value={this.state.user.email}
                            onChange={e => this.onChangeEmail(e.target.value)}/>
                        </div>
                        <br/>

                        <div className="button-wrapper">
                            <button className="saveChangesBtn" onClick={this.onSaveChanges}> Save </button>
                            <button className="saveChangesBtn" onClick={this.onLogout}> Log out </button>
                        </div>

                    </div>

                    {this.renderRedirect()}

                </div>
            </div>
        );
    }

}



