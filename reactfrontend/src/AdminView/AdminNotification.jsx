import React from 'react';
import {style} from "./AdminNotification.css";
import {isValidEmail} from "../Validation";

export class AdminNotification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            notification : ''
        };

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeNotification = this.onChangeNotification.bind(this);
        this.sendNotification = this.sendNotification.bind(this);
        this.resetNotificationForm = this.resetNotificationForm.bind(this);
    }

    resetNotificationForm() {
        this.setState({email: ''});
        this.setState({notification: ''});
    }

    onChangeEmail(e){
        this.setState({email : e});
    }

    onChangeNotification(n){
        this.setState({notification : n});
    }

    sendNotification(){
        if(isValidEmail(this.state.email)) {
            let request = new XMLHttpRequest();
            request.open('POST', `http://localhost:8080/email`, false);
            request.setRequestHeader('content-type', 'application/json');
            let emailu = {
                "email": this.state.email,
                "text": this.state.notification
            };
            console.log(emailu);
            request.send(JSON.stringify(emailu));
            this.resetNotificationForm();
        }
    }

    render() {
        return (
            <div className="admin-notification-wrapper">
                <h3  className="admin-title">
                    Notification
                </h3>
                <div className="notification-form-container">
                    <div className="notification-form">

                        <div className="inputFieldNotificationAdmin">
                            <div className="label"> User Email: </div>
                            <input
                                className="inputFieldHospitalAdmin"
                                type="text"
                                ref="email"
                                value={this.state.email}
                                onChange={e => this.onChangeEmail(e.target.value)}/>
                        </div>

                        <div className="inputFieldNotificationAdmin">
                            <div className="label"> Notification: </div>
                            <div className="textAreaDiiv">
                            <textarea id="textareabox"
                                      name="textarea1"
                                      placeholder="Please write notification..."
                                      value={this.state.notification}
                                onChange={e => this.onChangeNotification(e.target.value)} cols="38" rows="6" />
                            </div>
                        </div>

                        <div>
                            <button className="sendButton" onClick={()=>{this.sendNotification()}}> Send</button>
                        </div>

                    </div>
                    <img src="https://i.pinimg.com/originals/0c/dc/d1/0cdcd1dae05650ce8157c2e0756632b2.png"
                        width="356"
                        height="356"
                    />
                </div>
            </div>
        );
    }

}




