import React from 'react';
import CanvasJSReact from './assets/canvasjs.react';
import {style} from  './Statistics.css';

let CanvasJSChart = CanvasJSReact.CanvasJSChart;

function createCommentAvrageData(label,y){
    return{label,y}
}


export class Statistics extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            datas: '',
        }
    }

    componentDidMount() {
        let request = new XMLHttpRequest();
        request.open('GET', `http://localhost:8080/stat`, true);
        //request.setRequestHeader('Access-Control-Allow-Origin');

        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                let rates = JSON.parse(request.response);
                let dataWrapper = [];
                Array.prototype.forEach.call(rates, onerating => {
                    dataWrapper.push(createCommentAvrageData(onerating.hotelname, onerating.rating));
                });
                this.setState({datas: dataWrapper});
            }
        };

        request.send();
    }

    render() {
        console.log(this.props.rats);

        const options = {
            title: {
                text: 'Hotel rating statistics'
            },
            theme: "dark2",
            animationEnabled: true,
            data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: this.state.datas
                }
            ]
        }


        return (
            <div className="back">
                <div className="chart2">
                    <CanvasJSChart options={options}
                        /* onRef={ref => this.chart = ref} */
                    />
                    {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
                </div>
            </div>
        );
    }
}
