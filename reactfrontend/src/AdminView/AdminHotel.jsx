import React from 'react';
import {style} from "./AdminHotel.css";

export class AdminHotel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false,
            hotel: {
                id: '',
                hname: '',
                owner_name: '',
                address: '',
                rating: 0,
                image: '',
            }
        };

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeOwnerName = this.onChangeOwnerName.bind(this);
        this.onChangeAddress = this.onChangeAddress.bind(this);
        this.onChangeImage = this.onChangeImage.bind(this);
        this.onSaveHotel = this.onSaveHotel.bind(this);
        this.onUpdateHotel = this.onUpdateHotel.bind(this);
        this.onDeleteHotel = this.onDeleteHotel.bind(this);
        this.resetHotelFormData = this.resetHotelFormData.bind(this);
    }

    resetHotelFormData() {
        this.setState({
            hotel: {
                id: '',
                ownerID: 1,
                hname: '',
                owner_name: '',
                address: '',
                rating : 0,
                image: '',
            }
        })
    }

    onChangeName(value) {
        this.setState(prevState => ({
            hotel: {
                ...prevState.hotel,
                hname: value
            }
        }));
    }

    onChangeOwnerName(value) {
        this.setState(prevState => ({
            hotel: {
                ...prevState.hotel,
                owner_name: value
            }
        }));
    }

    onChangeAddress(value) {
        this.setState(prevState => ({
            hotel: {
                ...prevState.hotel,
                address: value
            }
        }));
    }

    onChangeImage(value) {
        this.setState(prevState => ({
            hotel: {
                ...prevState.hotel,
                image: value
            }
        }));
    }

    onSaveHotel(){
        let request = new XMLHttpRequest();
        request.open('POST', `http://localhost:8080/hotel`, false);
        request.setRequestHeader('content-type', 'application/json');

        request.send(JSON.stringify(this.state.hotel));
    }

    onUpdateHotel(){
        let request = new XMLHttpRequest();
        request.open('PUT', `http://localhost:8080/hotelu`, false);
        request.setRequestHeader('content-type', 'application/json');

        request.send(JSON.stringify(this.state.hotel));

        this.resetHotelFormData();
    }

    onDeleteHotel(){
        let request = new XMLHttpRequest();
        request.open('DELETE', `http://localhost:8080/hotels/${this.state.hotel.hname}`, false);

        request.send();

        this.resetHotelFormData();
    }

    render() {
        return (
            <div className="admin-wrapper">
                <h3  className="admin-title">
                   Hotels
                </h3>

                { this.state.error &&
                <div className="error-message">
                    Looks like something went wrong!
                </div>
                }
                <div className="admin-hospital">

                    <div>
                        <img src="https://img.freepik.com/free-vector/hotel-background-design_1300-103.jpg?size=338&ext=jpg"
                             width="356"
                             height="356"
                        />
                    </div>
                    <div className="admin">
                        <div className="inputFieldHospitalAdmin">
                            <div className="label"> Name: </div>
                            <input
                                className="inputFieldHospitalAdmin"
                                type="text"
                                ref="name"
                                value={this.state.hotel.hname}
                                onChange={e => this.onChangeName(e.target.value)}/>
                        </div>

                        <br/>

                        <div className="inputFieldHospitalAdmin">
                            <div className="label"> Owner Name: </div>
                            <input
                                className="inputFieldHospitalAdmin"
                                type="text"
                                ref="name"
                                value={this.state.hotel.owner_name}
                                onChange={e => this.onChangeOwnerName(e.target.value)}/>
                        </div>

                        <br/>

                        <div className="inputFieldHospitalAdmin">
                            <div className="label"> Address: </div>
                            <input
                                className="inputFieldHospitalAdmin"
                                type="text"
                                ref="name"
                                value={this.state.hotel.address}
                                onChange={e => this.onChangeAddress(e.target.value)}/>
                        </div>

                        <br/>

                        <div className="inputFieldHospitalAdmin">
                            <div className="label"> Image: </div>
                            <input
                                className="inputFieldHospitalAdmin"
                                type="text"
                                ref="name"
                                value={this.state.hotel.image}
                                onChange={e => this.onChangeImage(e.target.value)}/>
                        </div>
                        <div className="functionalButtons">
                        <button className="saveHospitalBtn" onClick={this.onSaveHotel}> Save</button>
                        <button className="saveHospitalBtn" onClick={this.onUpdateHotel}> Update</button>
                        <button className="saveHospitalBtn" onClick={this.onDeleteHotel}> Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}




