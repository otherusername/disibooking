import React from 'react';
import {style} from "./Admin.css";
import {AdminHotel} from "./AdminHotel";
import {AdminNotification} from "./AdminNotification";
import {Redirect} from "react-router";

export class Admin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        };


        this.onLogout = this.onLogout.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={`/home`}/>
        }
    };

    onLogout(){
        this.setState({
            redirect: true
        });
        sessionStorage.setItem("loggedAdmin", "false");
    }

    render() {
        return (
            <div>
                {sessionStorage.getItem('loggedAdmin') === 'true' && sessionStorage.getItem('loggedUser') === 'admin@mail.com' &&
                <div className="admin-page">
                    <AdminHotel/>
                    <AdminNotification/>
                    <button className="logoutBtn" onClick={this.onLogout}> Log out</button>
                </div>
                }
                {this.renderRedirect()}
            </div>
        );
    }

}




