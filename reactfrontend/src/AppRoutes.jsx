import React from 'react';
import {Switch, Route, Router} from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import {Layout} from "./Layout";
import {Home} from "./Home/Home";
import {Admin} from "./AdminView/Admin";
import {ClientPage} from "./ClientView/ClientPage";
import {Hotels} from './Home/Hotels';
import {Register} from './ClientView/Register';
import {Statistics} from './AdminView/Statistics';
import {MyBookings} from './ClientView/MyBookings';

let history = [];

/**
 * @deprecated
 * @param address
 */
export const navigateTo = function (address) {
    if (global.isServer) {
        throw `Don't call navigateTo on server side`;
    }
    // if (!history && typeof window !== 'undefined' && window.history) {
    //     // window.history would only change the url
    //     // const stateObj = {};
    //     // const title = '';
    //     console.log('failing to navigate');
    //     // window.history.pushState(stateObj, title, address);
    // } else {
    console.log('navigation deprecated');
    history.push(address);
    // }
};

// globalClickHandler.init(navigateTo);

export class AppRoutes extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (<BrowserRouter>
            <Route path="/" render={() => {
                return (<Layout {...this.props}>
                    <Switch>
                        <Route path="/home" component={Home}/>
                        <Route path="/admin" component={Admin}/>
                        <Route path="/user/:email" component={ClientPage}/>
                        <Route path="/usernew" component={Register}/>
                        <Route path="/hotels" component={Hotels}/>
                        <Route path="/statistics" component={Statistics}/>
                        <Route path="/myreservations" component={MyBookings}/>
                        <Route path="*" component={Home}/>
                    </Switch>
                </Layout>);
            }}
            />
        </BrowserRouter>);
    }
}
